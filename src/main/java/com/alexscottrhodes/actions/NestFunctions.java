/**
	Thermostat for Echo
	Copyright (C) 2016  Alex Rhodes
	https://www.alexscottrhodes.com
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	Thermostat for Echo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
    
    Nest�, Nest Learning Thermostat�, Nest Leaf Logo� and the Works with Nest logos are trademarks licensed by Nest Labs, Inc.
    Echo, Alexa are trademarks licensed to Amazon.com, Inc. or its affiliates � 1996-2016
 */

package com.alexscottrhodes.actions;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import com.alexscottrhodes.nest.ErrorHandler;
import com.amazon.speech.slu.Intent;
import com.amazon.speech.slu.Slot;
import com.amazon.speech.speechlet.SpeechletException;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.SimpleCard;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * This class contains the main business logic for the Nest operations.
 * @author Alex Rhodes
 *
 */
public class NestFunctions {

	private static String thermName = "undefined";
	private static String thermId;
	private static String apiKey;
	private static String units;
	private static JsonObject model;
	 
	public static SpeechletResponse InitiateResponse(Intent intent) throws SpeechletException{
		
		/**
		 * Your thermostat ID and nest api key need to be added to these fields.
		 */
		thermId = "<Defualt Nest thermostat ID to use without specifying name first  here>";
		apiKey = "<Nest API key here>";
		
		HashMap<String,JsonObject> temp = queryDevice();
		if(!temp.containsKey("success")){
			return ErrorHandler.nestFailureResponse("Not found");
		}
		 
		model= temp.get("success");
		
		String intentName = (intent != null) ? intent.getName() : null;
		Slot thermNameSlot = intent.getSlot("THERMNAME");
			if(thermNameSlot.getValue() != null){
				String thermNameRaw = thermNameSlot.getValue();
				thermName = thermNameRaw;
				thermId =  nameToThermId(thermNameRaw);
				if(thermId.equals("not-found")){
					return ErrorHandler.failed("I'm sorry, I couldn't understand which thermostat you wanted me to control");
				}
			}else{
				model = model.get("devices").getAsJsonObject().get("thermostats").getAsJsonObject().get(thermId).getAsJsonObject();
			}
			
		units =	model.get("temperature_scale").getAsString().toLowerCase();
		
		if("NestIntent".equals(intentName)){
			return NestFunctions.tempCall(intent);
		}else if("NestTempQueryIntent".equals(intentName)){
			return NestFunctions.tempQuery();
		}else if("NestSingleIntent".equals(intentName)){
			return NestFunctions.singleTemp(intent);
		}else if("NestModeIntent".equals(intentName)){
			return NestFunctions.modeChange(intent);
		}else if("NestFanIntentOn".equals(intentName)){
			return NestFunctions.fanSwitch(true);
		}
		else if("NestFanIntentOff".equals(intentName)){
			return NestFunctions.fanSwitch(false);
		}
		else if("NestSpecIntent".equals(intentName)){
			return NestFunctions.specAdjustment(intent);
		}
		else if("NestQuickIntentUp".equals(intentName)){
			return NestFunctions.quickAdjust(true);
		}
		else if("NestQuickIntentDown".equals(intentName)){
			return NestFunctions.quickAdjust(false);
		}
		else if("NestWhereIntent".equals(intentName)){
			return NestFunctions.whereTempCall(intent);
		}else if("NestQuickFilterIntent".equals(intentName)){
			return quickFilter(intent);
		}
		else {
			throw new SpeechletException("Invalid Intent");
		}
	}
	
	
	public static SpeechletResponse whereTempCall(Intent intent){
		return ErrorHandler.failed(intent.getSlot("THERMNAME").getValue());
	}
	
	
/** 
 * Interprets the type of temperature call, whether it be range or single temperature
 * @param intent an Intent indicating a temperature request
 * @return a SpeechletResponse to the user
 */

	public static SpeechletResponse tempCall(Intent intent) {
		Slot lowSlot = intent.getSlot("LOWTEMP");
		Slot highSlot = intent.getSlot("HIGHTEMP");
		Slot rangeSlot = intent.getSlot("RANGEEND");
		return tempCall(lowSlot,highSlot,rangeSlot);
	}
	
	public static SpeechletResponse tempCall(Slot lowSlot, Slot highSlot, Slot rangeSlot){
		int low, high;
		//If temperature range
		if (lowSlot.getValue() != null && highSlot.getValue() != null) {
				
			//	Attempt to parse temperatures as ints
				try{
					low = Integer.parseInt(lowSlot.getValue());
					high = Integer.parseInt(highSlot.getValue());
				}catch(Exception e){ return ErrorHandler.failed("I'm sorry, I couldn't understand the temperatures you told me.");}
				
		return setTemp(low, high);
				
	//	If lowSlot is not null, indicating one side change
		} else if (lowSlot.getValue() != null) {
			//Try to parse temp as an integer
			try {
				low = Integer.parseInt(lowSlot.getValue());
			} catch (Exception e) {
				return ErrorHandler.failed("I'm sorry, I couldn't understand the temperature you told me.");
			}
		//	Make sure range end is defined 
			if (rangeSlot.getValue() != null) {
				String rangeVal = rangeSlot.getValue();
				if (rangeVal.equals("high") | rangeVal.equals("upper") | rangeVal.equals("top") | rangeVal.equals("higher") | rangeVal.equals("max") | rangeVal.equals("maximum")) {
					return setTemp(true, low);
				} else {
					return setTemp(false, low);
				}
			}else{
				return ErrorHandler.failed("I'm sorry, couldnt understand which temperature you wanted me to set. Please specify either high or low temperature");
			}
		}
		//If all else fails, return uknown error card.
		else {
			return ErrorHandler.failed("I'm sorry, I'm having trouble controlling your nest right now.");
		}
	}

	/**
	 * The following method sets the tempearutre between the given values
	 * @param low an Integer  indicating the low value for the temperature range in heat-cool mode
	 * @param high an Integer  indicating the high value for the temperature range in heat-cool mode
	 * @return a SpeecheltResponse for the user with a message repeating back the desired range so that he or she may ensure it was accurately interpreted
	 */
	public static SpeechletResponse setTemp(int low, int high) {
		String speechText ="";
		
		if(!thermName.equals("undefined")){
			speechText = "Okay, setting "+thermName+" temperature between " + low + " and " + high + ".";
			thermName = "undefined";
		}else{
		speechText = "Okay, setting temperature between " + low + " and " + high + ".";
		}
		String JSON = "";
			JSON = "{\"target_temperature_low_"+units+"\":" + low+ ",";
			JSON += "\"target_temperature_high_"+units+"\":" + high + "}";
			
			HashMap<String,String> result = nestCom(JSON);
			if(!result.get("response-code").equals("200")){
				return ErrorHandler.nestFailureResponse(result.get("message"));
			}

		// Create the plain text output.
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(speechText);
		return SpeechletResponse.newTellResponse(speech);
	}

	/**
	 * Sets either the high or low temperature of the range in heat-cool mode
	 * @param high a Boolean indicating if the temperature to be changed is the high (true) or low (false)
	 * @param temp an Integer of the desired temperature for the range
	 * @return a SpeecheltResponse for the user with a message repeating back the desired range so that he or she may ensure it was accurately interpreted
	 */
	public static SpeechletResponse setTemp(boolean high, int temp) {
		String speechText = "Okay, setting ";
		if(high){
			if(!thermName.equals("undefined")){
				speechText += "the high"+ thermName + " temperature to " + temp;
				thermName = "undefined";
			}else{
				speechText += "the high temperature to " + temp;
			}
		}else{
			if(!thermName.equals("undefined")){
				speechText += "the low "+ thermName + " temperature to " + temp;
				thermName = "undefined";
			}else{
				speechText += "the low temperature to " + temp;
			}
		}
	
		String JSON;
		if(high){
			JSON = "{\"target_temperature_high_"+units+"\":" + temp+ "}";
			
		}else{
			JSON = "{\"target_temperature_low_"+units+"\":" + temp+ "}";
		}
		HashMap<String,String> result = nestCom(JSON);
		if(!result.get("response-code").equals("200")){ 
			return ErrorHandler.nestFailureResponse(result.get("message"));
		}
		
		// Create the plain text output.
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(speechText);
		return SpeechletResponse.newTellResponse(speech);
	}

	/**
	 * Returns an information response to the user including the current temperature and settings
	 * @return a SpeecheltResponse with the current temperature and setting information
	 */
	public static SpeechletResponse tempQuery() {
		String speechText;
		if(!thermName.equals("undefined")){
			speechText = "The temperature in the " + thermName + " is ";
			thermName = "undefined";
		}else{
			speechText = "The temperature inside is ";
		}
		int low,high,target;
		
		Double ambientD = Double.parseDouble(model.get("ambient_temperature_"+units).getAsString());
		int ambient = (int) Math.round(ambientD);
		String state = model.get("hvac_state").getAsString();
		if(model.get("hvac_mode").getAsString().equals("heat-cool")){
			Double lowd = Double.parseDouble(model.get("target_temperature_low_"+units).getAsString());
			low = (int)Math.round(lowd);
			
			Double highd = Double.parseDouble(model.get("target_temperature_high_"+units).getAsString());
			high = (int)Math.round(highd);
			
			speechText += ambient +" degrees. Nest is in heating and cooling mode, set to a low of " + low + " and a high of " + high + ". Currently, Nest is " + state;
		}else{
			Double targetd = Double.parseDouble(model.get("target_temperature_"+units).getAsString());
			target = (int) Math.round(targetd);
			
			speechText += ambient + " degrees. Nest is in " + model.get("hvac_mode").getAsString() + " mode, set to a temperature of "+ target + ". Currently, nest is "+ state;
		}
		// Create the plain text output.
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(speechText);
		return SpeechletResponse.newTellResponse(speech);
	}
	
	
	/**
	 * Sets a temperature when in heat or cool mode
	 * @param intent an Intent indicating a single temperature change
	 * @return SpeecheltResponse for the user with a message repeating back the desired range so that he or she may ensure it was accurately interpreted
	 */
	public static SpeechletResponse singleTemp(Intent intent) {
		String speechText;
		int temp;
		try{
		String tempString = intent.getSlot("TEMPVAL").getValue();
			temp = Integer.parseInt(tempString);
		}catch(Exception e){
			speechText = "I'm sorry, I couldn't understand the temperature you requested.";
			return ErrorHandler.failed(speechText);
		} 
		
		
		String	JSON = "{\"target_temperature_"+units+"\":"+temp+"}";
		HashMap<String,String> result = nestCom(JSON);
		if(!result.get("response-code").equals("200")){
			return ErrorHandler.nestFailureResponse(result.get("message"));
		}
		
		if(!thermName.equals("undefined")){
			speechText = "Ok,I'm setting the "+ thermName +" nest temperature to " + temp + " degrees.";
			thermName = "undefined";
		}else{
			speechText = "Ok,I'm setting the temperature to " + temp + " degrees.";
		}
		// Create the plain text output.
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(speechText);
		return SpeechletResponse.newTellResponse(speech);
	}
	
	/**
	 * A method to change between thermostat modes, either heat, cool, heat-cool or off
	 * @param intent an Intent indicating a mode change request
	 * @return a SpeecheltResponse for the user with a message repeating back the desired range so that he or she may ensure it was accurately interpreted
	 */
	public static SpeechletResponse modeChange(Intent intent){
		String speechText;
		String mode,modePlain;
		try{
			mode = intent.getSlot("MODE").getValue();
			if(mode == null){
				throw new Exception();
			}
			modePlain = mode;
		}catch(Exception e){
			speechText = "I'm sorry, I couldn't understand what mode you wanted. Please specify heat, cool, heat-cool or off.";
			return ErrorHandler.failed(speechText);
		}
		if(mode.equals("AC") | mode.equals("cooling") | mode.equals("cool")){
			mode  = "cool";
		}
		else if(mode.equals("heating") | mode.equals("heat")){
			mode = "heat";
		}
		else if(mode.equals("heat-cool") | mode.equals("heat cool")  | mode.equals("high-low") | mode.equals("high low") | mode.equals("range")){
			mode = "heat-cool";
		}
		else if(mode.equals("off")){
			mode="off";
		}else{
			speechText = "I'm sorry, I couldn't understand what mode you wanted. Please specify heat, cool, heat-cool or off.";
			return ErrorHandler.failed(speechText);
		}
		
		String JSON ="";
		if(mode.equals("cool")){
			JSON = "{\"hvac_mode\":\"cool\"}";
		}else if(mode.equals("heat")){
			JSON = "{\"hvac_mode\":\"heat\"}";
		}else if(mode.equals("heat-cool")){
			JSON = "{\"hvac_mode\":\"heat-cool\"}";
		}else if(mode.equals("off")){
			JSON = "{\"hvac_mode\":\"off\"}";
		}
		
		HashMap<String,String> result = nestCom(JSON);
		if(!result.get("response-code").equals("200")){
			return ErrorHandler.nestFailureResponse(result.get("message"));
		}
		if(!thermName.equals("undefined")){
			speechText = "Ok, I'm setting the "+ thermName + " mode to " + modePlain;
			thermName = "undefined";
		}else{
			speechText = "Ok,I'm setting the mode to " + modePlain;
		}
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(speechText);
		return SpeechletResponse.newTellResponse(speech);
	}
	
	/**
	 * Sets the fan on or off
	 * @param on a Boolean indicating if the fan is to be turned on or shut off
	 * @return a SpeecheltResponse for the user with a message repeating back the desired range so that he or she may ensure it was accurately interpreted
	 */
	public static SpeechletResponse fanSwitch(boolean on){
		String speechText;
		if(!thermName.equals("undefined")){
			speechText = "Ok,I'm turning the "+thermName+" fan ";
			thermName = "undefined";
		}else{
			speechText = "Ok,I'm turning the fan ";
		}
		String JSON = "";
		if(on){
			speechText += "on";
			JSON = "{\"fan_timer_active\": true}";
		}
		else{
			speechText += "off";
			JSON= "{\"fan_timer_active\": false}";
		}
		
		HashMap<String,String> result = nestCom(JSON);
		if(!result.get("response-code").equals("200")){
			return ErrorHandler.nestFailureResponse(result.get("message"));
		}
				
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(speechText);
		return SpeechletResponse.newTellResponse(speech);
	}
	
	
	/**
	 * A utility method to determine which direction a user desired the temperature to move in a "quick" 2 degree adjustment
	 * @param intent an Intent indicating a "quick" adjustment intent 
	 * @return a SpeecheltResponse for the user with a message repeating back the desired range so that he or she may ensure it was accurately interpreted
	 */
	public static SpeechletResponse quickFilter(Intent intent){
		Slot directionSlot = intent.getSlot("DIRECTION");
		if(directionSlot.getValue()==null){
			return ErrorHandler.failed("I'm sorry, I couldn't understand what direction you wanted me to move the temperature");
		}
		
		String direction = directionSlot.getValue();
		if(direction.equals("raise") | direction.equals("increase") | direction.equals("up") | direction.equals("higher") | direction.equals("heat") | direction.equals("heat up") | direction.equals("warmer") | direction.equals("hotter")){
			return tempAdjuster(2, "higher");
		}else{
			return tempAdjuster(2, "lower");
		}
	}
	
	/**
	 * Method to quickly adjust temperature by two degrees with a simple "quick" request
	 * @param warmer a Boolean to indicate if the temperature should be increased or decreased
	 * @return a SpeecheltResponse for the user with a message repeating back the desired range so that he or she may ensure it was accurately interpreted
	 */
	public static SpeechletResponse quickAdjust(boolean warmer){
		if(warmer){
			return tempAdjuster(2,"higher");
		}else{
			return tempAdjuster(2, "lower");
		}
	}
	
	/**
	 * Method to adjust the temperature a given direction and specified number of degrees. Will adjust either a single temperature or shift the range in heat-cool mode.
	 * @param intent an Intent indicating a directional temperature command
	 * @return a SpeecheltResponse for the user with a message repeating back the desired range so that he or she may ensure it was accurately interpreted
	 */
	
	public static SpeechletResponse specAdjustment(Intent intent){
		Slot degreeSlot = intent.getSlot("DEGREES");
		Slot directionSlot = intent.getSlot("DIRECTION");
		
		if (degreeSlot.getValue() == null){
			return ErrorHandler.failed("I'm sorry, I couldn't understand the number of degrees you told me.");
		}
		if (directionSlot.getValue() == null){
			return ErrorHandler.failed("I'm sorry, I couldn't understand which way you wanted me to move the temperature.");
		}
		
		String direction = directionSlot.getValue();
		int degrees;
		try{
			degrees = Integer.parseInt(degreeSlot.getValue());
		}catch(Exception e){
			return ErrorHandler.failed("I'm sorry, I couldn't understand the number of degrees you told me.");
		}
		return tempAdjuster(degrees,direction);
	}
	
	/**
	 * A method for adjusting the temperature the given amount in the given direction
	 * @param degrees an Integer representing the desired number of degrees
	 * @param direction the direction to move the temperature
	 * @return a SpeecheltResponse for the user with a message repeating back the desired range so that he or she may ensure it was accurately interpreted
	 */
	public static SpeechletResponse tempAdjuster(int degrees, String direction){
		String speechText = "";
		String currentMode = model.get("hvac_mode").getAsString();
		String JSON = "";
		if(currentMode.equals("heat") | currentMode.equals("cool")){
			try{
				float oldTargetF = model.get("target_temperature_"+units).getAsFloat();
				int oldTarget = Math.round(oldTargetF);
				int newTarget;
				if(direction.equals("raise") | direction.equals("increase") | direction.equals("up") | direction.equals("higher") | direction.equals("heat") | direction.equals("heat up") | direction.equals("warmer") | direction.equals("hotter")){
						newTarget = oldTarget + degrees;
					if(!thermName.equals("undefined")){
						speechText = "Ok, I'm raising the " + thermName + " temperature " + degrees + " degrees to " + newTarget;
						thermName = "undefined";
					}else{
						speechText = "Ok, I'm raising the temperature " + degrees + " degrees to " + newTarget;
					}
				}else{
					newTarget = oldTarget - degrees;
					if(!thermName.equals("undefined")){
						speechText = "Ok, I'm lowering the " + thermName + " temperature " + degrees + " degrees to " + newTarget;
						thermName = "undefined";
					}else{
						speechText = "Ok, I'm lowering the temperature " + degrees + " degrees to " + newTarget;
					}
				}
				JSON = "{\"target_temperature_"+units+"\":"+newTarget+"}"; 
			}catch(Exception e){
				return ErrorHandler.failed("I'm sorry, I'm having trouble adjusting the temperature for you.");
			}
		
		}else{
			try{
				float oldfh = model.get("target_temperature_high_"+units).getAsFloat();
				float oldfl = model.get("target_temperature_low_"+units).getAsFloat();
				int oldLow = Math.round(oldfl);
				int oldHigh = Math.round(oldfh);
				if(direction.equals("raise") | direction.equals("increase") | direction.equals("up") | direction.equals("higher") | direction.equals("heat") | direction.equals("heat up") | direction.equals("warmer") | direction.equals("hotter")){
					int newHigh = oldHigh + degrees;
					int newLow = oldLow + degrees;
					JSON = "{\"target_temperature_low_"+units+"\":" + newLow+ ",";
					JSON += "\"target_temperature_high_"+units+"\":" + newHigh + "}";
					if(!thermName.equals("undefined")){
						speechText = "Ok, I'm shifting the " + thermName + " temperature up " + degrees + " degrees to " + newLow + " and "+ newHigh;
						thermName = "undefined";
					}else{
						speechText = "Ok, I'm shifting the temperature up " + degrees + " degrees to " + newLow + " and "+ newHigh;
					}
					
				}else{
					int newHigh = oldHigh - degrees;
					int newLow = oldLow - degrees;
					JSON = "{\"target_temperature_low_"+units+"\":" + newLow+ ",";
					JSON += "\"target_temperature_high_"+units+"\":" + newHigh + "}";
					if(!thermName.equals("undefined")){
						speechText = "Ok, I'm shifting the " + thermName + " temperature down " + degrees + " degrees to " + newLow + " and "+ newHigh;
						thermName = "undefined";
					}else{
						speechText = "Ok, I'm shifting the temperature down " + degrees + " degrees to " + newLow + " and "+ newHigh;
					}
				}
			}catch(Exception e){
				return ErrorHandler.failed("I'm sorry, I'm having trouble adjusting the temperature for you.");
			}
			
		}
		HashMap<String,String> result = nestCom(JSON);
		if(!result.get("response-code").equals("200")){
			return ErrorHandler.nestFailureResponse(result.get("message"));
		}
				
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(speechText);
		return SpeechletResponse.newTellResponse(speech);
	}
	
	/**
	 * A method for sending a failure response to the user
	 * @param message a String with the method-specific failure message to read to the user
	 * @param cardContent a String with content for a card to be passed with the response
	 * @return a SpeecheltResponse with an informative failure message to the user
	 */
	public static SpeechletResponse failedCard(String message,String cardContent){
		SimpleCard card = new SimpleCard();
		card.setTitle("Support");
		card.setContent(cardContent);
		
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(message);
		return SpeechletResponse.newTellResponse(speech,card);
	}
	
	/**
	 * A method that builds a model of the device's current state
	 * @return a HashMap with data about the device keyed by the name of the respective field
	 */
	private static HashMap<String, JsonObject> queryDevice(){
		HashMap<String, JsonObject> modelMap = new HashMap<String,JsonObject>();

		try{
			String address = "https://developer-api.nest.com?auth="+apiKey;
		    URL url = new URL(address);
		    System.out.println("URL:" + address);
		    HttpURLConnection request = (HttpURLConnection) url.openConnection();
		    request.connect();
		    JsonParser jp = new JsonParser(); 
		    if(request.getResponseCode()!=200){
		        JsonObject resp = jp.parse(new InputStreamReader((InputStream) request.getErrorStream())).getAsJsonObject();
			    modelMap.put(resp.get("message").getAsString(),null);
			    return modelMap;
		    }
		    JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent())); 
		    JsonObject thermObject = root.getAsJsonObject();
		    model = thermObject;
		    modelMap.put("success", model);
		    return modelMap;
		}catch(Exception e){
				e.printStackTrace();
				return null;
		}
	}
	

	/**
	 * A method that sends data to the Nest via the Nest API
	 * @param JSON a JSON string of the fields to be updated
	 * @return a HashMap containing the response information from the request
	 */
	private static HashMap<String,String> nestCom(String JSON) {
			HashMap<String,String> result = new HashMap<String,String>();
		try {
			String address = "https://developer-api.nest.com/devices/thermostats/"+thermId+"?auth="+apiKey;
			System.out.println(address);
			System.out.println(JSON);
			URL url = new URL(address);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setRequestMethod("PUT");
	        connection.setDoOutput(true);
	        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
	        osw.write(String.format(JSON));
	        osw.flush();
	        osw.close();
	        
	        result.put("response-code", ""+connection.getResponseCode());
	        
	        if(connection.getResponseCode() != 200){
		        JsonParser jp = new JsonParser();
		        JsonObject resp = jp.parse(new InputStreamReader((InputStream) connection.getErrorStream())).getAsJsonObject();
			    result.put("message",resp.get("message").getAsString());
		    }
		    else{
		    	result.put("message", "ok");
		    }
		    return result;
		} catch (Exception e) {
			e.printStackTrace();
			result.put("response-code", "-1");
			result.put("message","I'm sorry, I wasn't able to control your Nest right now.");
			return result;
		}
	}

	/**
	 * A utility for identifying the thermostat ID based on the name passed by the user
	 * @param thermNameRaw a String representing the thermostat's raw name
	 * @return a String of the thermostats ID within the Nest structure model
	 */
	@SuppressWarnings("rawtypes")
	public static String nameToThermId(String thermNameRaw){
		String result = "not-found";
		
			JsonObject thermRoot = model.get("devices").getAsJsonObject().get("thermostats").getAsJsonObject();
			Iterator it = thermRoot.entrySet().iterator();
			while(it.hasNext()){
				Map.Entry pair = (Map.Entry) it.next();
				String thermId = (String) pair.getKey();
				JsonObject thermObject = ((JsonObject) pair.getValue()).getAsJsonObject();
				String name = thermObject.get("name").getAsString();
				thermNameRaw = thermNameRaw.toLowerCase();
				name = name.toLowerCase();
				if(name.contains(thermNameRaw)){
					model = thermObject;
					thermName = thermNameRaw;
					return thermId;
				}
			}
		return result;
	}
	

}
