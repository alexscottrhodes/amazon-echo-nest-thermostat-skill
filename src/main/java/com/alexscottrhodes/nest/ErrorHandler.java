/**
	Thermostat for Echo
	Copyright (C) 2016  Alex Rhodes
	https://www.alexscottrhodes.com
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	Thermostat for Echo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
    
    Nest�, Nest Learning Thermostat�, Nest Leaf Logo� and the Works with Nest logos are trademarks licensed by Nest Labs, Inc.
    Echo, Alexa are trademarks licensed to Amazon.com, Inc. or its affiliates � 1996-2016
 */

package com.alexscottrhodes.nest;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.PlainTextOutputSpeech;

/**
 * This class handles the Nest error responses as documented in the Nest API documentation
 * @author Alex Rhodes
 *
 */
public class ErrorHandler {

	/**
	 * Sends a failure response with a given message
	 * @param message a String of the message to provide with the failure response
	 * @return a SpeechletResponse with the failure message
	 */
	public static SpeechletResponse failed(String message){
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(message);
		return SpeechletResponse.newTellResponse(speech);
	}
	
	/**
	 * Returns an informative message that handles the Nest error messages as described in the Nest API documentation
	 * @param message a String of the Nest failure response 
	 * @return a SpeechletResponse with an informative message for the user to understand why a failure occured.
	 */
	public static SpeechletResponse nestFailureResponse(String message){

		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		
		
		message=message.toLowerCase();
		
		if(message.contains("no write permission")){
			message = "I'm sorry, I do not have permission to control that feature of your Nest.";
		}
		
		else if(message.contains("internal error")){
			message = "I'm sorry, there was a problem with Nest's services. Please try again later.";
		}
		
		else if(message.contains("unauthorized")){
			//Detailed response here
			message = "I'm sorry, I do not have authorization to control that feature of your nest. Please make sure the Nest is present on your Nest account, and still activated through the Thermostat for Echo website.";
		}
		
		else if(message.contains("forbidden")){
			//Detailed response here
			message = "I'm sorry, I do not have authorization to control that feature of your nest. Please make sure the Nest is present on your Nest account, and still activated through the Thermostat for Echo website.";
		}
		
		else if(message.contains("service unavailable")){
			message = "I'm sorry, there was a problem with Nest's services. Please try again later.";	
		}
		
		else if(message.contains("internal error")){
			message = "I'm sorry, there was a problem with Nest's services. Please try again later.";
		}
		
		else if(message.contains("blocked")){
			message = "I'm sorry, Nest only allows me to control your Nest a limited number of times in a short period. Please try again later.";
		}
		
		else if(message.contains("not writeable")){
			message = "I'm sorry, I can not contorl that feature of your Nest";
		}
		
		else if(message.contains("of wrong type")){
			message = "I'm sorry, I could not understand the instruction you gave me.";
		}
		
		else if(message.contains("invalid thermostat id")){
			message = "I'm sorry, I do not have authorization to control that feature of your nest. Please make sure the Nest is present on your Nest account, and still activated through the Thermostat for Echo website.";
		}
		
		else if(message.contains("is not a valid request")){
			message = "I'm sorry, I could not understand the instruction you gave me.";
		}
		
		else if(message.contains("cannot set c and f temperatures simultaneously")){
			message = "I'm sorry, I can not set both Celsius and Fahrenheit at the same time";
		}
		
		
		else if(message.contains("cannot set") && message.contains("while mode is")){
			
			String mode = "x";
			String temp = "x";
			
			if(message.contains("target_temperature_high")){
				temp = "a temperature range";
			}
			else if(message.contains("target_temperature_low")){
				temp = "a temperature range";
			}
			else if(message.contains("target_temperature")){
				temp = "an individual temperature";
			}
			
			if(message.contains("heat-cool")){
				mode = "heat-cool";
			}
			else if(message.contains("heat")){
				mode = "heat";
			}
			else if(message.contains("cool")){
				mode = "cool";
			}else if(message.contains("off")){
				mode="off";
			}
			
			if(mode.equals("off")){
				message = "I'm sorry, I can not set " + temp +" while your Nest is " + mode +". Please change modes first.";
			}else{
				message = "I'm sorry, I can not set " + temp +" while your Nest is in " + mode +" mode.  Please specify either high or low temperature, or change modes.";
			}
			if(mode.equals("x") | temp.equals("x")){
				message="I'm sorry, I couldn't set the temperature for you.";
			}
			
		}
		
		else if(message.contains("cannot set") && message.contains("closer than")){
			message = "I'm sorry, I can't set the temperature range that close together.";
		}
		
		else if(message.contains("low") && message.contains("value")){
			message = "I'm sorry, I can not set the temperature that low";
		}
		
		else if(message.contains("high") && message.contains("value")){
			message = "I'm sorry, I can not set the temperature that high";
		}
		
		else if(message.contains("low") && message.contains("lock")){
			message = "I'm sorry, your nest has a locked temperature preventing me from setting the temperature that low.";
		}
		
		else if(message.contains("high") && message.contains("lock")){
			message = "I'm sorry, your nest has a locked temperature preventing me from setting the temperature that high.";
		}
		
		else if(message.contains("larger than")){
			message = "I'm sorry, I can't set the temperature that way, " + message;
		}
		
		else if(message.contains("energy-saving-events")){
			message = "I'm sorry, I can't change the mode when your Nest is in an energy savings event";
		}
		
		else if(message.contains("thermostat lock is enabled")){
			message = "I'm sorry, your thermostat is locked.";
		}
		
		else if(message.contains("cannot change hvac mode to")){
			message = "I'm sorry, your Nest doesn't support that mode";
		}
		
		else if(message.contains("invalid hvac mode")){
			message = "I'm sorry, your Nest doesn't support that mode";
		}
		else if(message.contains("while structure is away")){
			message = "I'm sorry, your Nest is in Away mode";
		}
		
		else if(message.contains("invalid thermostat id")){
			message = "I'm sorry, I can't reach the Nest you previously activated with your account. Please log in to www.ThermostatForEcho.com to manage your devices.";
		}
		else if(message.equals("not found")){
			message = "I'm sorry, I can't reach the Nest you previously activated with your account. Please log in to www.ThermostatForEcho.com to manage your devices.";
		}	
		
		else if(message.equals("no hvac fan")){
			message = "I'm sorry, you do not have a fan that Nest can control.";
		}
		else if(message.equals("cannot activate fan during smoke")){
			message = "I'm sorry, I can not activate your fan, your Nest is in a safety shutoff mode.";
		}
		else{
			System.out.println(message);
			message = "I'm sorry, I was unable to control your nest right now. Please try again later. If the problem persists, please email support@thermostatforecho.com";
			
		}
			speech.setText(message);
			return SpeechletResponse.newTellResponse(speech);
		}
	
	}

