/**
	Thermostat for Echo
	Copyright (C) 2016  Alex Rhodes
	https://www.alexscottrhodes.com
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	Thermostat for Echo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
    
    Nest�, Nest Learning Thermostat�, Nest Leaf Logo� and the Works with Nest logos are trademarks licensed by Nest Labs, Inc.
    Echo, Alexa are trademarks licensed to Amazon.com, Inc. or its affiliates � 1996-2016
 */
package com.alexscottrhodes.nest;

import com.alexscottrhodes.actions.NestFunctions;
import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.LaunchRequest;
import com.amazon.speech.speechlet.Session;
import com.amazon.speech.speechlet.SessionEndedRequest;
import com.amazon.speech.speechlet.SessionStartedRequest;
import com.amazon.speech.speechlet.Speechlet;
import com.amazon.speech.speechlet.SpeechletException;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.Reprompt;

public class NestSpeechlet implements Speechlet{

	//private static final Logger log = LoggerFactory.getLogger(EricaSpeechlet.class);
	
	public void onSessionStarted(SessionStartedRequest request, Session session) throws SpeechletException {
		//Initialization functions
	}

	public SpeechletResponse onLaunch(LaunchRequest request, Session session) throws SpeechletException {
		//Initial greeting/launch
		//log.info("onLaunch requestId={}, sessionId={}", request.getRequestId(), session.getSessionId());
		return getGreetingResponse();
	}

	public SpeechletResponse onIntent(IntentRequest request, Session session) throws SpeechletException {
		Intent intent = request.getIntent();
		String intentName = (intent != null) ? intent.getName() : null;

		
		if ("AMAZON.HelpIntent".equals(intentName)) {
			return getHelpResponse(); 
		}else if("AMAZON.CancelIntent".equals(intentName)){
			return exitResponse();
		}
		else if("GreetingIntent".equals(intentName)){
			return getGreetingResponse();
		}else {
			return NestFunctions.InitiateResponse(intent);
		}
	}

	public void onSessionEnded(SessionEndedRequest request, Session session) throws SpeechletException {
		//At the end of a session
	}
	
	
	//Core logic 
	
	//Initial greeting
	private SpeechletResponse getGreetingResponse() {
		String speechText = "Hello, I'm going to help Alexa control your Nest thermostat. How can I help you?";


		// Create the plain text output.
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(speechText);

		// Create reprompt
		Reprompt reprompt = new Reprompt();
		reprompt.setOutputSpeech(speech);

		return SpeechletResponse.newAskResponse(speech, reprompt);
	}
	
	private SpeechletResponse exitResponse() {
		String speechText = "Goodbye.";


		// Create the plain text output.
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(speechText);
		SpeechletResponse sr = new SpeechletResponse();
		sr.setOutputSpeech(speech);
		sr.setShouldEndSession(true);
		return sr;
	}
	
	//Help response
	private SpeechletResponse getHelpResponse() {
		String speechText = "Thermostat can control your temperature and mode settings, or tell what your Nest is doing. You can say things like: set the temperature to seventy one degrees, switch to heating mode. How can I help you?";

		// Create the plain text output.
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(speechText);

		// Create reprompt
		Reprompt reprompt = new Reprompt();
		reprompt.setOutputSpeech(speech);

		return SpeechletResponse.newAskResponse(speech, reprompt);
	}
	

}
