/**
	Thermostat for Echo
	Copyright (C) 2016  Alex Rhodes
	https://www.alexscottrhodes.com
	
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	Thermostat for Echo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. 
    
    Nest�, Nest Learning Thermostat�, Nest Leaf Logo� and the Works with Nest logos are trademarks licensed by Nest Labs, Inc.
    Echo, Alexa are trademarks licensed to Amazon.com, Inc. or its affiliates � 1996-2016
 */

package com.alexscottrhodes.nest;

import java.util.HashSet;
import java.util.Set;
import com.amazon.speech.speechlet.lambda.SpeechletRequestStreamHandler;

/**
 * Handles the Alexa Skill Service request
 * @author Alex Rhodes
 * 
 */
public final class NestRequestHandler extends SpeechletRequestStreamHandler {
    
	private static final Set<String> supportedApplicationIds = new HashSet<String>();
    static {
    	/**
    	 * Your Amazon application ID must be provided here
    	 */
        supportedApplicationIds.add("<Amazon application ID here>");
    }
    
    /**
     * Instantiates a nest speechlet with the given application IDs
     */
    public NestRequestHandler() {
        super(new NestSpeechlet(), supportedApplicationIds);
    }
}
